package ru.t1.dkandakov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.dkandakov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.dkandakov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.dkandakov.tm.dto.request.project.*;
import ru.t1.dkandakov.tm.dto.request.user.UserLoginRequest;
import ru.t1.dkandakov.tm.dto.response.project.*;
import ru.t1.dkandakov.tm.dto.response.user.UserLoginResponse;
import ru.t1.dkandakov.tm.enumerated.ProjectSort;
import ru.t1.dkandakov.tm.enumerated.Status;
import ru.t1.dkandakov.tm.marker.ISoapCategory;
import ru.t1.dkandakov.tm.model.Project;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Category(ISoapCategory.class)
public final class ProjectEndpointTest {

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @Nullable
    private String token;

    @Before
    public void init() {
        @Nullable final UserLoginResponse loginResponse = authEndpoint.login(
                new UserLoginRequest("test", "test")
        );
        token = loginResponse.getToken();
    }

    @Test
    public void projectCreate() {
        @NotNull final String projectName = "TestProject";
        @NotNull final String descriptionName = "TestProject";
        Assert.assertThrows(Exception.class, () -> projectEndpoint.createProject(
                new ProjectCreateRequest())
        );
        Assert.assertThrows(Exception.class, () -> projectEndpoint.createProject(
                new ProjectCreateRequest(null, null, null))
        );
        Assert.assertThrows(Exception.class, () -> projectEndpoint.createProject(
                new ProjectCreateRequest(UUID.randomUUID().toString(), projectName, descriptionName))
        );
        @Nullable ProjectCreateResponse createResponse = projectEndpoint.createProject(
                new ProjectCreateRequest(token, projectName, descriptionName)
        );
        Assert.assertNotNull(createResponse);
        Assert.assertNotNull(createResponse.getProject());
        Assert.assertEquals(projectName, createResponse.getProject().getName());
    }

    @Test
    public void projectRemoveById() {
        Assert.assertThrows(Exception.class, () -> projectEndpoint.removeProjectById(
                new ProjectRemoveByIdRequest())
        );
        Assert.assertThrows(Exception.class, () -> projectEndpoint.removeProjectById(
                new ProjectRemoveByIdRequest(null, null))
        );
        Assert.assertThrows(Exception.class, () -> projectEndpoint.removeProjectById(
                new ProjectRemoveByIdRequest(UUID.randomUUID().toString(), "Id"))
        );
        final int projectsLength = 10;
        @Nullable List<Project> projects = new ArrayList<>();
        for (int i = 0; i < projectsLength; i++) {
            @NotNull final String testProjectName = "TestProject_" + i;
            @Nullable ProjectCreateResponse createResponse = projectEndpoint.createProject(
                    new ProjectCreateRequest(token, testProjectName, "description")
            );
            projects.add(createResponse.getProject());
        }
        for (int i = 0; i < projectsLength; i++) {
            @Nullable final Project project = projects.get(i);
            projectEndpoint.removeProjectById(new ProjectRemoveByIdRequest(token, project.getId()));
            Assert.assertNull(projectEndpoint.getProjectById(new ProjectGetByIdRequest(token, project.getId())).getProject());
        }
    }

    @Test
    public void projectRemoveByIndex() {
        @NotNull final String name = "ProjectForRemove";
        @NotNull final String description = "DescriptionForRemove";
        @NotNull final ProjectCreateRequest createRequest = new ProjectCreateRequest(token, name, description);
        projectEndpoint.createProject(createRequest);
        @NotNull final Integer index = projectEndpoint.listProject(
                new ProjectListRequest(token, null)).getProjects().size() - 1;
        @NotNull final ProjectRemoveByIndexRequest request = new ProjectRemoveByIndexRequest(token, index);
        projectEndpoint.removeProjectByIndex(request);
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.getProjectByIndex(new ProjectGetByIndexRequest(token, index)));
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.removeProjectByIndex(null));
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.removeProjectByIndex(new ProjectRemoveByIndexRequest(null, index)));
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.removeProjectByIndex(new ProjectRemoveByIndexRequest("a", index)));
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.removeProjectByIndex(new ProjectRemoveByIndexRequest(token, index)));
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.removeProjectByIndex(new ProjectRemoveByIndexRequest(token, null)));
    }

    @Test
    public void projectUpdateById() {
        @NotNull final String testProjectName = "TestProject";
        Assert.assertThrows(Exception.class, () -> projectEndpoint.updateProjectById(
                new ProjectUpdateByIdRequest(null, null, null, null)
        ));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.updateProjectById(
                new ProjectUpdateByIdRequest(UUID.randomUUID().toString(), null, null, null)
        ));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.updateProjectById(
                new ProjectUpdateByIdRequest(token, "Id", null, null)
        ));
        @Nullable ProjectCreateResponse createResponse = projectEndpoint.createProject(
                new ProjectCreateRequest(token, testProjectName, "description")
        );
        Assert.assertNotNull(createResponse);
        Assert.assertNotNull(createResponse.getProject());
        @Nullable ProjectUpdateByIdResponse updateResponse = projectEndpoint.updateProjectById(
                new ProjectUpdateByIdRequest(token, createResponse.getProject().getId(), "testName", "testDescription")
        );
        Assert.assertNotNull(updateResponse);
        Assert.assertNotNull(updateResponse.getProject());
        Assert.assertNotEquals(createResponse.getProject().getName(), updateResponse.getProject().getName());
        Assert.assertNotEquals(createResponse.getProject().getDescription(), updateResponse.getProject().getDescription());
        Assert.assertEquals(createResponse.getProject().getId(), updateResponse.getProject().getId());
    }

    @Test
    public void projectList() {
        @NotNull final String testProjectName = "TestProject";
        Assert.assertThrows(Exception.class, () -> projectEndpoint.listProject(
                new ProjectListRequest())
        );
        Assert.assertThrows(Exception.class, () -> projectEndpoint.listProject(
                new ProjectListRequest(null, null))
        );
        Assert.assertThrows(Exception.class, () -> projectEndpoint.listProject(
                new ProjectListRequest("", null))
        );
        Assert.assertThrows(Exception.class, () -> projectEndpoint.listProject(
                new ProjectListRequest(UUID.randomUUID().toString(), null))
        );
        @Nullable ProjectCreateResponse createResponse = projectEndpoint.createProject(
                new ProjectCreateRequest(token, testProjectName, "description")
        );
        Assert.assertNotNull(createResponse);
        Assert.assertNotNull(createResponse.getProject());
        @Nullable ProjectListResponse response = projectEndpoint.listProject(
                new ProjectListRequest(token, ProjectSort.BY_DEFAULT)
        );
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getProjects());
    }

    @Test
    public void changeProjectStatusById() {
        @NotNull final String testProjectName = "TestProject";
        Assert.assertThrows(
                Exception.class, () -> projectEndpoint.changeProjectStatusById(
                        new ProjectChangeStatusByIdRequest())
        );
        Assert.assertThrows(
                Exception.class, () -> projectEndpoint.changeProjectStatusById(
                        new ProjectChangeStatusByIdRequest(null, null, null))
        );
        Assert.assertThrows(
                Exception.class, () -> projectEndpoint.changeProjectStatusById(
                        new ProjectChangeStatusByIdRequest(UUID.randomUUID().toString(), null, null))
        );
        Assert.assertThrows(
                Exception.class, () -> projectEndpoint.changeProjectStatusById(
                        new ProjectChangeStatusByIdRequest(token, null, null))
        );
        Assert.assertThrows(
                Exception.class, () -> projectEndpoint.changeProjectStatusById(
                        new ProjectChangeStatusByIdRequest(token, testProjectName, null))
        );
        Assert.assertThrows(
                Exception.class, () -> projectEndpoint.changeProjectStatusById(
                        new ProjectChangeStatusByIdRequest(token, UUID.randomUUID().toString(), Status.IN_PROGRESS))
        );
        @Nullable final ProjectCreateResponse projectCreateResponse = projectEndpoint.createProject(
                new ProjectCreateRequest(token, testProjectName, "description")
        );
        Assert.assertNotNull(projectCreateResponse);
        Assert.assertNotNull(projectCreateResponse.getProject());

        @Nullable final ProjectChangeStatusByIdResponse response = projectEndpoint.changeProjectStatusById(
                new ProjectChangeStatusByIdRequest(token, projectCreateResponse.getProject().getId(), Status.IN_PROGRESS)
        );
        Assert.assertNotNull(response);
        Assert.assertEquals(Status.IN_PROGRESS, response.getProject().getStatus());
    }

    @Test
    public void changeProjectStatusByIndex() {
        Assert.assertThrows(
                Exception.class, () -> projectEndpoint.changeProjectStatusByIndex(
                        new ProjectChangeStatusByIndexRequest())
        );
        Assert.assertThrows(
                Exception.class, () -> projectEndpoint.changeProjectStatusByIndex(
                        new ProjectChangeStatusByIndexRequest(null, null, null))
        );
        Assert.assertThrows(
                Exception.class, () -> projectEndpoint.changeProjectStatusByIndex(
                        new ProjectChangeStatusByIndexRequest(UUID.randomUUID().toString(), null, null))
        );
        Assert.assertThrows(
                Exception.class, () -> projectEndpoint.changeProjectStatusByIndex(
                        new ProjectChangeStatusByIndexRequest(null, 0, Status.IN_PROGRESS))
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.changeProjectStatusByIndex(
                        new ProjectChangeStatusByIndexRequest(token, 0, null))
        );

        @Nullable final ProjectCreateResponse projectCreateResponse = projectEndpoint.createProject(
                new ProjectCreateRequest(token, "testProjectName", "description")
        );
        Assert.assertNotNull(projectCreateResponse);
        Assert.assertNotNull(projectCreateResponse.getProject());

        @NotNull final Integer index = projectEndpoint.listProject(
                new ProjectListRequest(token, null)).getProjects().size() - 1;

        @Nullable final ProjectChangeStatusByIndexResponse response = projectEndpoint.changeProjectStatusByIndex(
                new ProjectChangeStatusByIndexRequest(token, index, Status.IN_PROGRESS)
        );
        Assert.assertNotNull(response);
        Assert.assertEquals(Status.IN_PROGRESS, response.getProject().getStatus());

        @Nullable final ProjectChangeStatusByIndexResponse responseTwo = projectEndpoint.changeProjectStatusByIndex(
                new ProjectChangeStatusByIndexRequest(token, 0, Status.NOT_STARTED)
        );
        Assert.assertNotNull(response);
        Assert.assertEquals(Status.NOT_STARTED, responseTwo.getProject().getStatus());
        @NotNull final ProjectRemoveByIndexRequest request = new ProjectRemoveByIndexRequest(token, index);
        projectEndpoint.removeProjectByIndex(request);

    }

    @Test
    public void projectClear() {
        Assert.assertThrows(Exception.class, () -> projectEndpoint.clearProject(
                new ProjectClearRequest())
        );
        Assert.assertThrows(Exception.class, () -> projectEndpoint.clearProject(
                new ProjectClearRequest(null))
        );
        Assert.assertThrows(Exception.class, () -> projectEndpoint.clearProject(
                new ProjectClearRequest(UUID.randomUUID().toString()))
        );
        final int projectsLength = 10;
        for (int i = 0; i < projectsLength; i++) {
            @NotNull final String testProjectName = "TestProject_" + i;
            projectEndpoint.createProject(new ProjectCreateRequest(token, testProjectName, "description"));
        }
        @Nullable ProjectClearResponse response = projectEndpoint.clearProject(
                new ProjectClearRequest(token)
        );
        @Nullable ProjectListResponse projectList = projectEndpoint.listProject(
                new ProjectListRequest(token, ProjectSort.BY_DEFAULT)
        );
        Assert.assertNotNull(response);
    }

}
