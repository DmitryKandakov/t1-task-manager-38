package ru.t1.dkandakov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.api.repository.ITaskRepository;
import ru.t1.dkandakov.tm.api.service.IConnectionService;
import ru.t1.dkandakov.tm.api.service.ITaskService;
import ru.t1.dkandakov.tm.enumerated.Status;
import ru.t1.dkandakov.tm.exception.entity.StatusEmptyException;
import ru.t1.dkandakov.tm.exception.field.*;
import ru.t1.dkandakov.tm.model.Task;
import ru.t1.dkandakov.tm.repository.TaskRepository;

import java.sql.Connection;
import java.util.Collections;
import java.util.List;

public final class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    public ITaskRepository getRepository(@NotNull final Connection connection) {
        return new TaskRepository(connection);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ITaskRepository repository = getRepository(connection);
            repository.add(userId, task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ITaskRepository repository = getRepository(connection);
            repository.add(userId, task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ITaskRepository repository = getRepository(connection);
            repository.update(task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new IndexIncorrectException();
        @NotNull final Task task = findOneByIndex(userId, index);
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ITaskRepository repository = getRepository(connection);
            repository.update(task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task changeTaskStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final Status status) {
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (index == null || index < 0)
            throw new IndexIncorrectException();
        if (status == null)
            throw new StatusEmptyException();
        @NotNull final Task task = findOneByIndex(userId, index);
        task.setStatus(status);
        task.setUserId(userId);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ITaskRepository repository = getRepository(connection);
            repository.update(task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task changeTaskStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (id == null || id.isEmpty())
            throw new TaskIdEmptyException();
        if (status == null)
            throw new StatusEmptyException();
        @NotNull final Task task = findOneById(userId, id);
        task.setStatus(status);
        task.setUserId(userId);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ITaskRepository repository = getRepository(connection);
            repository.update(task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId.isEmpty()) return Collections.emptyList();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final ITaskRepository repository = getRepository(connection);
            return repository.findAllByProjectId(userId, projectId);
        }
    }

}

