package ru.t1.dkandakov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.api.constant.IDBConstant;
import ru.t1.dkandakov.tm.api.repository.IRepository;
import ru.t1.dkandakov.tm.comparator.CreatedComparator;
import ru.t1.dkandakov.tm.comparator.StatusComparator;
import ru.t1.dkandakov.tm.exception.entity.EntityNotFoundException;
import ru.t1.dkandakov.tm.model.AbstractModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final Connection connection;

    public AbstractRepository(@NotNull Connection connection) {
        this.connection = connection;
    }

    protected abstract String getTableName();

    @NotNull
    protected String getSortType(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return IDBConstant.CREATED;
        if (comparator == StatusComparator.INSTANCE) return IDBConstant.STATUS;
        else return IDBConstant.NAME;
    }

    @NotNull
    public abstract M fetch(@NotNull final ResultSet row) throws Exception;

    @NotNull
    public abstract M add(@NotNull final M model);

    @NotNull
    @Override
    @SneakyThrows
    public Collection<M> add(@NotNull final Collection<M> models) {
        List<M> result = new ArrayList<>();
        for (M model : models) {
            result.add(add(model));
        }
        return result;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) {
        removeAll();
        return add(models);
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return findAll(null);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull String query;
        if (comparator == null) query = String.format("SELECT * FROM %s", getTableName());
        else query = String.format("SELECT * FROM %s ORDER BY %s", getTableName(), getSortType(comparator));
        try (@NotNull final Statement statement = connection.createStatement();
             @NotNull final ResultSet resultSet = statement.executeQuery(query)
        ) {
            while (resultSet.next()) result.add(fetch(resultSet));
            return result;
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@Nullable final String id) {
        @NotNull final String query = String.format("SELECT * FROM %s WHERE %s = ? LIMIT 1;", getTableName(), IDBConstant.ROW_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)
        ) {
            statement.setString(1, id);
            try (@NotNull final ResultSet rowSet = statement.executeQuery()) {
                if (!rowSet.next()) throw new EntityNotFoundException();
                return fetch(rowSet);
            }
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneByIndex(@Nullable final Integer index) {
        if (index == null) return null;
        @NotNull final String sql = "SELECT * FROM " + getTableName() + " WHERE LIMIT 1 OFFSET ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1, index);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        @Nullable final M entity = fetch(resultSet);
        statement.close();
        return entity;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeOne(@Nullable final M model) {
        @NotNull final String query = String.format("DELETE FROM %s WHERE %s = ?;", getTableName(), IDBConstant.ROW_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)
        ) {
            statement.setString(1, model.getId());
            statement.executeUpdate();
            connection.commit();
        }
        return model;
    }

    @Nullable
    @Override
    public M removeOneById(@NotNull final String id) {
        @Nullable final M model = findOneById(id);
        if (model == null) return null;
        removeOne(model);
        return model;
    }

    @Nullable
    @Override
    public M removeOneByIndex(@NotNull final Integer index) {
        @Nullable final M model = findOneByIndex(index);
        if (model == null) return null;
        removeOne(model);
        return model;
    }

    @Override
    public void removeAll(@Nullable final Collection<M> collection) {
        if (collection == null) return;
        for (M m : collection) {
            removeOne(m);
        }
    }

    @Override
    @SneakyThrows
    public void removeAll() {
        @NotNull final String query = String.format("DELETE FROM %s;", getTableName());
        try (@NotNull final Statement statement = connection.createStatement()) {
            statement.executeUpdate(query);
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public int getSize() {
        @NotNull final String query = String.format("SELECT COUNT(1) FROM %s;", getTableName());
        try (@NotNull final Statement statement = connection.createStatement();
             @NotNull final ResultSet resultSet = statement.executeQuery(query)) {
            resultSet.next();
            return resultSet.getInt("count");
        }
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return findOneById(id) != null;
    }

}