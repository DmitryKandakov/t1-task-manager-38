package ru.t1.dkandakov.tm.api.constant;

import org.jetbrains.annotations.NotNull;

public interface IDBConstant {

    @NotNull
    String TABLE_USER = "tm_user";

    @NotNull
    String TABLE_SESSION = "tm_session";

    @NotNull
    String TABLE_PROJECT = "tm_project";

    @NotNull
    String TABLE_TASK = "tm_task";

    @NotNull
    String ROW_ID = "row_id";

    @NotNull
    String USER_ID = "user_id";

    @NotNull
    String CREATED = "created";

    @NotNull
    String STATUS = "status";

    @NotNull
    String NAME = "name";

    @NotNull
    String LOGIN = "login";

    @NotNull
    String PASSWORD_HASH = "password_hash";

    @NotNull
    String EMAIL = "email";

    @NotNull
    String FST_NAME = "fst_name";

    @NotNull
    String LST_NAME = "lst_name";

    @NotNull
    String MDL_NAME = "mdl_name";

    @NotNull
    String ROLE = "role";

    @NotNull
    String LOCK_FLG = "lock_flg";

    @NotNull
    String PROJECT_NAME = "name";

    @NotNull
    String PROJECT_DESCRIPTION = "description";

    @NotNull
    String PROJECT_CREATED = "created";

    @NotNull
    String PROJECT_STATUS = "status";

    @NotNull
    String TASK_NAME = "name";

    @NotNull
    String TASK_DESCRIPTION = "description";

    @NotNull
    String TASK_CREATED = "created";

    @NotNull
    String TASK_STATUS = "status";

    @NotNull
    String TASK_PROJECT_ID = "project_id";

}
