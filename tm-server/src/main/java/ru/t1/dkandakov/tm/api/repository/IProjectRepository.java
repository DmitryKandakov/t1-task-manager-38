package ru.t1.dkandakov.tm.api.repository;

import ru.t1.dkandakov.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {
}
